package P10_2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 11/1/13
 * Time: 9:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class ButtonFrame1 extends JFrame {
    private static final int FRAME_WIDTH = 100;
    private static final int FRAME_HEIGHT = 60;
    private static int times;
    public ButtonFrame1() {
        createComponents();
        setSize(FRAME_WIDTH,FRAME_HEIGHT);
    }
    private void createComponents() {
        JButton button = new JButton("Click me!");
        JPanel panel = new JPanel();
        panel.add(button);
        add(panel);

        ActionListener listener = new ClickListener();
        button.addActionListener(listener);
    }

    class ClickListener implements ActionListener  {

        @Override
        public void actionPerformed(ActionEvent e) {
            times++;
            System.out.println("I was clicked  " + times + "  times!");
        }
    }

}
