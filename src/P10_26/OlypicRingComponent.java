package P10_26;

import javax.swing.JComponent;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 11/1/13
 * Time: 10:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class OlypicRingComponent extends JComponent{
    @Override
    protected void paintComponent(Graphics g) {
        drawRing(g, 100, 100, 100, 100, Color.CYAN);
        drawRing(g, 200, 100, 100, 100, Color.BLUE);
        drawRing(g, 300, 100, 100, 100, Color.RED);
        drawRing(g, 150, 150, 100, 100, Color.YELLOW);
        drawRing(g, 250, 150, 100, 100, Color.GREEN);

    }
    public void drawRing( Graphics g, int x, int y, int width, int height, Color color) {
        Graphics2D g2d = (Graphics2D)g;
        g2d.setStroke(new BasicStroke(10));
        g2d.setColor(color);
        g2d.drawOval(x,y, width,height);
    }
}
