package P10_26;
import javax.swing.JFrame;
import javax.swing.JComponent;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 11/1/13
 * Time: 10:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class OlympicRingDriver {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setSize(600, 400);
        frame.setTitle("Olympic Ring");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JComponent component = new OlypicRingComponent();
        frame.add(component);
        frame.setVisible(true);
    }
}
