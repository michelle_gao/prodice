package pig;

import javax.swing.*;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 11/2/13
 * Time: 4:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class AutoPlayer extends Player {
    private boolean buttonEnable=true;
    @Override
    public void play(JButton rollButton, JButton holdButton,JTextField currentPlayerF,
                 JTextField winnerText){
        Random rdm = new Random();
        int playTimes = (rdm.nextInt(10) +1);
        int diceValue;
        for (int i = 1; i < playTimes; i++) {
            int diceRoll = roll();

            if (diceRoll == 1) {
                setScore(0);
                playerScore.setText("0");
                break;
            }
            else {
                score +=diceRoll;
                playerScore.setText(Integer.toString(score));
                if (score>= 100) {

                    winnerText.setText(name);
                }
            }

        }
        Player currentPlayer = getOpponent();
        currentPlayerF.setText(currentPlayer.name);
        rollButton.setEnabled(true);
        holdButton.setEnabled(true);
        currentPlayer.play(rollButton, holdButton, currentPlayerF,
            winnerText);
    }
}
