package pig;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 11/2/13
 * Time: 4:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class DiceGame {
    private JPanel mPanel;
    private JButton rollButton;
    private JButton holdButton;
    private JTextField player1Score;
    private JTextField player1;
    private JTextField player2Score;
    private JButton STARTButton;
    private JButton ENDButton;   // this button has not been worked on yet.
    private JTextField player2;
    private JTextField currentPlayerF;
    private JTextField winnerName;
    private HumanPlayer person;
    private AutoPlayer computer;
    private Player currentPlayer;

    public static void main(String[] args) {
        JFrame frame = new JFrame("DiceGame");
        frame.setContentPane(new DiceGame().mPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(600,400);
        frame.setVisible(true);
    }
    public DiceGame() {
        rollButton.setEnabled(false);
        holdButton.setEnabled(false);
        ENDButton.setEnabled(false);
        STARTButton.addActionListener(new startListener());
        rollButton.addActionListener(new rollListener());
        holdButton.addActionListener(new holdListener());
    }


    class startListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            person = new HumanPlayer();
            person.setName("Michelle");
            player1.setText(person.name);
            person.playerScore = player1Score;
            computer = new AutoPlayer();
            computer.setName("Computer");
            player2.setText(computer.name);
            computer.playerScore = player2Score;
            person.setOpponent(computer);
            computer.setOpponent(person);
            Random rdm = new Random();
            int turn = rdm.nextInt(2);
            currentPlayer = turn==0?computer:person;
            ENDButton.setEnabled(true);
            currentPlayerF.setText(currentPlayer.getName());
            if (currentPlayer instanceof HumanPlayer) {
                rollButton.setEnabled(true);
                holdButton.setEnabled(true);
            }
            currentPlayer.play(rollButton, holdButton,currentPlayerF,
                    winnerName);
        }
    }
    class rollListener implements ActionListener  {

        @Override
        public void actionPerformed(ActionEvent e) {
           person.play(rollButton, holdButton,currentPlayerF,
                   winnerName);
        }
    }

    class holdListener implements ActionListener   {

        @Override
        public void actionPerformed(ActionEvent e) {
            currentPlayer = computer;
            currentPlayerF.setText(currentPlayer.getName());
            currentPlayer.play(rollButton, holdButton,currentPlayerF,
                    winnerName);
        }
    }




}
