package pig;

import javax.swing.*;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 11/2/13
 * Time: 4:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class HumanPlayer extends Player {
    private boolean buttonEnable=false;
    @Override
    public void play(JButton rollButton, JButton holdButton,JTextField currentPlayerF,JTextField winnerText){

        int diceRoll = roll();

        if (diceRoll == 1) {
            setScore(0);
            playerScore.setText("0");

            rollButton.setEnabled(buttonEnable);
            holdButton.setEnabled(buttonEnable);
            Player currentPlayer = getOpponent();
            currentPlayerF.setText(currentPlayer.name);
            currentPlayer.play(rollButton, holdButton,
                    currentPlayerF,winnerText);
        }
        else {
            score += diceRoll;
            playerScore.setText(Integer.toString(score));
            if (score >= 100) {
                winnerText.setText(name);
            }
        }


    }
}
