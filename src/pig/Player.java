package pig;

import javax.swing.*;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 11/2/13
 * Time: 4:29 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Player {
    protected String name;
    protected int score;
    public JTextField playerScore;
    protected Player opponent;
    private boolean buttonEnable;


    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Player getOpponent() {
        return opponent;
    }

    public void setOpponent(Player opponent) {
        this.opponent = opponent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Player(){
        score = 0;
    };

    public Player(String name) {
        this.name = name;
    }

    public enum StatusValue{
        WAIT, PLAY
    }
    public int roll() {
        Random rdm = new Random();
        return rdm.nextInt(6) + 1;
    }

    public abstract void play(JButton rollButton, JButton holdButton,JTextField currentPlayerF,
                              JTextField winnerText);

}
