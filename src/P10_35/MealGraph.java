package P10_35;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 11/1/13
 * Time: 11:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class MealGraph {
    private JPanel mPanel;

    private JButton ham;
    private JButton soup;
    private JButton beef;
    private JButton salad;
    private JButton coke;
    private JButton chicken;
    private JButton noodle;
    private JButton sprite;
    private JButton tuna;
    private JTextField textField1;
    private JButton  extraMeal;
    private JTextArea Bill;
    private JButton selmon;
    private JTextArea bill;
    private JButton moreMealButton;

    private static double mealBill=0;
    private ActionListener mActionListener;
    private ActionListener fieldActionListener;





    public static void main(String[] args) {
        JFrame frame = new JFrame("MealBill");
        frame.setContentPane(new MealGraph().mPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


    public MealGraph() {
        mActionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String strButtonText =  ((JButton)e.getSource()).getText();
                moreMeal(e, strButtonText);
            }
        };
        fieldActionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String strButtonText =  textField1.getText();
                moreMeal(e, strButtonText);
            }
        };
        ham.addActionListener(mActionListener);
        soup.addActionListener(mActionListener);
        beef.addActionListener(mActionListener);
        salad.addActionListener(mActionListener);
        coke.addActionListener(mActionListener);
        chicken.addActionListener(mActionListener);
        noodle.addActionListener(mActionListener);
        sprite.addActionListener(mActionListener);
        tuna.addActionListener(mActionListener);
        selmon.addActionListener(mActionListener);
        moreMealButton.addActionListener(fieldActionListener);
    }

    public void moreMeal(ActionEvent e, String strButtonText) {

        try {
                double mealPrice = Double.parseDouble(strButtonText);
                mealBill += mealPrice;
                double tax = mealPrice * 0.1;
                DecimalFormat numberFormat = new DecimalFormat("#.00");
                double dTip = mealPrice * 0.15;
                String tip = numberFormat.format(dTip);

                String totalBill = numberFormat.format(mealBill + tax + dTip);
                bill.setText("");
                bill.append("Total Meal Cost:" + String.valueOf(mealBill) + "\n");
                bill.append("Tax:" + String.valueOf(tax)+ "\n");
                bill.append("Tip:" + tip + "\n");
                bill.append("TotalBill:" + totalBill + "\n");
        } catch (NumberFormatException e1) {
        bill.setText("");

        }
    }


}