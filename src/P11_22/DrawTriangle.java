package P11_22;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 11/2/13
 * Time: 2:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class DrawTriangle {
    private JPanel mPanel;
    int nClickes=0;
    private ArrayList<Point> mPoints = new ArrayList<Point>();

    public static void main(String[] args) {
        JFrame frame = new JFrame("DrawRectangle");
        frame.setContentPane(new DrawTriangle().mPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(600,400);
        frame.setVisible(true);
    }

    public DrawTriangle() {
        mPanel.addMouseListener(new triangleListener());
    }

    class triangleListener extends MouseAdapter {
        @Override
        public void mousePressed(MouseEvent e) {
            drawTriangle(e);
        }
    }

    public void drawTriangle(MouseEvent e) {;
        nClickes++;
        Point CurrentPoint = e.getPoint();
        switch (nClickes) {
            case 1:
                mPanel.getGraphics().fillOval(CurrentPoint.x, CurrentPoint.y, 3, 3);
                mPoints.clear();
                mPoints.add(CurrentPoint);
                break;

            case 2:
                mPanel.getGraphics().drawLine(CurrentPoint.x, CurrentPoint.y, mPoints.get(0).x, mPoints.get(0).y);
                mPoints.add(CurrentPoint);
                break;
            case 3:
                mPanel.getGraphics().drawLine(CurrentPoint.x, CurrentPoint.y, mPoints.get(0).x, mPoints.get(0).y);
                mPanel.getGraphics().drawLine(CurrentPoint.x, CurrentPoint.y, mPoints.get(1).x, mPoints.get(1).y);
                break;
            case 4:
                mPanel.repaint();
                nClickes = 0;
                break;
            default:
                break;

        }
    }
}


